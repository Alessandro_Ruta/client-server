/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strikeball;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author If you are not me you have copied
 */
public class JavaClient {
    public static void main(String[] args){
        Socket connessione;
        String server = "localhost";
        int porta = 3500;
        try {
            connessione = new Socket(server, porta);
            System.out.println("Connessione aperta   :)    ");
            
            Client client = new Client(connessione);
            client.start();
            
            connessione.close();
            System.out.println("Connessione chiusa   :(   ");
        } catch (IOException ex) {
            System.err.println("Errore nella connssione");
        }
        }             
    }