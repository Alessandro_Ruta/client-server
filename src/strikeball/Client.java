/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strikeball;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aleru
 */
public class Client extends Thread{
    Socket connessione;
    BufferedReader in;
    PrintWriter out;
    BufferedReader riga;
    InputStreamReader reader;
    String messaggio;
    String leggoDaTastiera;
    String[] tentativo;
    
    public Client(Socket connessione) throws IOException{
        this.connessione=connessione;
        messaggio=null;
        leggoDaTastiera=null;
        tentativo= new String[4];
        reader= new InputStreamReader(System.in);
        riga = new BufferedReader(reader);
        in = new BufferedReader(new InputStreamReader(this.connessione.getInputStream()));
        out = new PrintWriter(this.connessione.getOutputStream(), true);
    }
    
    @Override
    public void run(){
        try {
            try {
                messaggio=in.readLine();
                System.out.println("Il server ha scritto: "+messaggio);
                System.out.println("Inserisci il codice di 4 cifre");
                leggoDaTastiera=riga.readLine();
                tentativo=leggoDaTastiera.split("");
                out.println(tentativo);
            } catch (IOException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            in.close();
            out.close();
            reader.close();
            riga.close();
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
