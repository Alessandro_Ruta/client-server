/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strikeball;

/**
 *
 * @author aleru
 */
public class Partita {
    int[] codiceInt;
    String[] codice;
    String[] tentalo;
    int balls;
    int striks;
    int tentativi;
    int stayTuned;
    
    public Partita(){
        codiceInt = new int[4]; 
        tentalo = new String[4];
        codice = new String[4];
        balls=0;
        striks=0;
        tentativi=12;
        stayTuned=0;
        for(int n=0; n==4; n++){
            codiceInt[n]= (int) (Math.random() * 10);
        }
        for(int n=0; n==4; n++){
            codice[n]= ""+codiceInt[n];
        }
        
        
    }
    
    public void confrontalo(String numero1, String numero2, String numero3, String numero4){
        this.tentalo[0]=numero1;
        this.tentalo[1]=numero2;
        this.tentalo[2]=numero3;
        this.tentalo[4]=numero4;
        
        for(int n=0; n<4; n++){
            if(this.tentalo[n].equals(this.codice[n])){
                this.striks++;
            }
        }
        
        for(int n=0; n<4; n++){
            for (int x=0; x<4; x++){
                if(tentalo[n].equals(codice[x])){
                this.balls++;
                }
            }
        }
        
        if(striks==4)
            stayTuned=1;
        if(tentativi==0)
            stayTuned=-1;
    }

    public int[] getCodiceInt() {
        return codiceInt;
    }

    public int getBalls() {
        return balls;
    }

    public int getStriks() {
        return striks;
    }

    public int getTentativi() {
        return tentativi;
    }
    
    public int getStayTuned() {
        return stayTuned;
    }
}
