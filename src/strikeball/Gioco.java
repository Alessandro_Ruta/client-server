/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strikeball;

import java.io.*;
import static java.lang.Math.random;
import static java.lang.StrictMath.random;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Informatica
 */
    class Gioco extends Thread{
	// dichiarazione delle variabili socket e dei buffer
	Socket connessione;
	BufferedReader input;
	PrintWriter output;
        InputStreamReader reader;
        BufferedReader myInput;
        String str;
        String message;
        int messaggio;
        String[] codice;
        Partita partita;
        
	public Gioco(Socket connessione) throws IOException
	{
		this.connessione = connessione;
                input = new BufferedReader(new InputStreamReader(connessione.getInputStream()));
                output = new PrintWriter(connessione.getOutputStream(), true);  
                reader = new InputStreamReader (System.in);
                myInput = new BufferedReader (reader);
                str= new String();
                codice = new String[4];
                codice=null;
                messaggio=0;
                message=null;
                partita= new Partita();
	}

        @Override
	public void run()
	{
            try {
                System.out.println("Connsessione stabilita   :)   ");
                System.out.println("Sto servendo il client che ha indirizzo "+connessione.getInetAddress());
                while(partita.getStayTuned()==0){
                    output.println("Indovina 4 numeri. \n"
                            + "Hai cancora" +partita.getTentativi() + "\n"
                                    + "Nel caso tu abbia problemi scrivi ''help''\n");
                    try {
                        message=input.readLine();
                        codice=input.readLine().split("");
                    } catch (IOException ex) {
                        System.err.println("Errore nell'input");
                    }
                    
                    
                    partita.confrontalo(codice[0], codice[1], codice[3], codice[4]);
                    
                }
                
                if(partita.getStayTuned()==-1){
                    output.println("hey...qualcuno qui ha perso? so er boss finale!");
                }
                if(partita.getStayTuned()==1){
                    output.println("hey...sei er boss finale, hai vinto er biscottino!");
                }
                
                input.close();
                output.close();
                myInput.close();
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Gioco.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
	
	}
        
        
}