/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strikeball;
import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Informatica
 */
public class StrikeBall {

    public static void main(String args[]){
            
        ServerSocket server;
        int tempo=10000;
        int porta=3500;
        
        
        while(true){
            try {
                server = new ServerSocket(porta);
                server.setSoTimeout(tempo);
                Socket connessione;
                System.out.println("Sto aspettando connessioni...");
                Contatore contatore = new Contatore(tempo);
                contatore.start();
                connessione=server.accept();
                contatore.interrompi();
                Gioco gioco = new Gioco(connessione);
                connessione.close();
                server.close();
            } catch (IOException ex) {
                Logger.getLogger(StrikeBall.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
                       
                
}
}